package com.github.zagum.expandicon.entry.slice;

import com.github.zagum.expandicon.ExpandIconView;

import com.github.zagum.expandicon.entry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.TouchEvent;

public class MainAbilitySlice extends AbilitySlice {
    private Text click;
    private Component touch;
    private ExpandIconView expandIconView1;
    private ExpandIconView expandIconView2;
    private ExpandIconView expandIconView3;
    private ExpandIconView expandIconView4;

    private float touchY;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        click = (Text) findComponentById(ResourceTable.Id_click);
        expandIconView1 = (ExpandIconView) findComponentById(ResourceTable.Id_expand_icon1);
        expandIconView2 = (ExpandIconView) findComponentById(ResourceTable.Id_expand_icon2);
        expandIconView3 = (ExpandIconView) findComponentById(ResourceTable.Id_expand_icon3);
        expandIconView4 = (ExpandIconView) findComponentById(ResourceTable.Id_expand_icon4);

        touch = (Component) findComponentById(ResourceTable.Id_touch);

        touch.setTouchEventListener(
                new Component.TouchEventListener() {
                    @Override
                    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                        float fraction;
                        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                            touchY = touchEvent.getPointerPosition(0).getY();
                        } else if (touchEvent.getAction() == TouchEvent.POINT_MOVE) {
                            if (Math.signum(touchEvent.getPointerPosition(0).getY() - touchY) < 0) {
                                fraction = 1f;
                            } else {
                                fraction = 0f;
                            }
                            expandIconView1.setFraction(fraction, true);
                            expandIconView2.setFraction(fraction, true);
                            expandIconView4.setFraction(fraction, true);
                            touchY = touchEvent.getPointerPosition(0).getY();
                        } else if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                            expandIconView1.setFraction(.5f, true);
                            expandIconView2.setFraction(.5f, true);
                            expandIconView4.setFraction(.5f, true);
                        }
                        return true;
                    }
                });

        click.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        expandIconView3.switchState();
                    }
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
