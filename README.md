# Demo

<img src="art/demo.gif" width="40%"/>

# Used

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:ohos-ExpandIcon:1.0.1'
}
```

```xml

<com.github.zagum.expandicon.ExpandIconView
    ohos:id="$+id:expand_icon1"
    ohos:height="24vp"
    ohos:width="24vp"
    ohos:layout_alignment="horizontal_center"
    ohos:top_margin="16vp"
    app:eiv_animationDuration="300"
    app:eiv_color="#000"
    app:eiv_roundedCorners="false"
    app:eiv_switchColor="false"
/>
```

 ```java
    expandIconView1 = (ExpandIconView)findComponentById(ResourceTable.Id_expand_icon1);
    expandIconView1.setFraction(1f, true);
    expandIconView1.setFraction(0f, true);
    expandIconView1.switchState();

```

# API:
## class ExpandIconView
**public void switchState()**
- description: switch state

**public void switchState(boolean animate)**
- description: switch state with animate

**public void setFraction(float fraction, boolean animate)**
- description: set fraction with animate

**public void setAnimationDuration(long animationDuration)**
- description: set animation duration

**public void setState(int state, boolean animate)**
- description: set state with animate

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| eiv_roundedCorners | boolean | set rounded corners
| eiv_switchColor | color | set switch color
| eiv_color | color | set default color
| eiv_colorMore | color | set more color
| eiv_colorLess | color | set less color
| eiv_colorIntermediate | color | set intermediate color
| eiv_animationDuration | long | set animation duration
| eiv_lineWidth | dimension | set line width
| eiv_padding | dimension | set padding


License
-------

    Copyright 2016 Evgenii Zagumennyi

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.